# opendata-bristol

Location for development of Open Data project for Bristol Open Data, it's currently called Dotty.

This tiny development was put together by Phil D Hall from Elzware Ltd against a very small budget to give a template/framework for interested parties to develop moving forwards. Or to develop further if budget becomes available to do such.

Assuming you are in the Gitlab project you can get Phil from there, he's a member. Or touch him on phildhall@elzware.com if you fancy, either works.

In a nutshell, what we have here is an evolution of a standard ChatScript system with a small number of layers to match user's inputs to website pages.

Smalltalk is available but has been turned off as the next step for this system is a formal testing phase and which point smalltalk can be reinitialised. 

We also have a fledgling conversational function here where missing data can be fed back to data owners, but this again need; testing, training and focus before it should be used in any kind of production sense.

#####
# Please note, this is not a live system. The WIP UI is available from https://www.elzware.com/open_data/ and registration credentials should be requested from credentials.dotty@elzware.com
#####

THIS IS NOT A PRODUCTION SYSTEM AND SHOULD NOT BE USED IN ANY PRODUCTION ENVIRONMENT UNTIL ADDITIONAL STEPS HAVE BEEN TAKEN TO CONTROL THE TEMPLATE AND A FORMAL PROCESS HAS BEEN UNDERTAKEN TO DIAL THE TEMPLATE INTO THE DESTINATION SITE OR SYSTEM.

ELZWARE WILL NOT BE HELD LIABLE FOR ANY MATTERS OF ANY SORT UNLESS IT IS KEPT INFORMATION OF ADDITIONAL DEVELOPMENTAL ACTIVITIES AND ITS NAME SHOULD NOT BE USED UNLESS BY PRIOR AGREEMENT.

Sorry for all the shouting, but better be clear and all that.

Let's put in a working MIT license too, just for good measure.

The MIT License (MIT)
Copyright (C) 2002 - 2020 by Phil D Hall aka Elzware Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Last updated by Phil D Hall 20200806
